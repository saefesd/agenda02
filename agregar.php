<?php
// Incluimos la configuracion y conexion a la MySQL.
include('config.php');
// Definimos la variable $msg por seguridad.
$msg = "";
// Si se apreta el boton Agendar, da la condicion como true.
if($_POST['agendar'])
{
	// Verificamos que no alla ningun dato sin rellenar.
	if(!empty($_POST['nombre']) || !empty($_POST['apellidos']) || !empty($_POST['direccion']) || !empty($_POST['email']) 
	|| !empty($_POST['telefono']))
	{
		// Pasamos los datos de los POST a Variables, y le ponemos seguridad.
		$nombre = htmlentities($_POST['nombre']);
		$apellidos = htmlentities($_POST['apellidos']);
		$direccion = htmlentities($_POST['direccion']);
		$email = htmlentities($_POST['email']);
		$telefono = htmlentities($_POST['telefono']);
		$foto = htmlentities($_POST['foto']);
		// Insertamos los datos en la base de datos, si da algun error lo muestra. 
		$sql = "INSERT INTO personas (nombre, apellidos, direccion, email, telefono, foto) VALUES ('".$nombre."','".$apellidos."','".$direccion."',
										'".$email."','".$telefono."', '".$foto."')";
		mysql_query($sql,$link) or die(mysql_error());
		// Mostramos un mensaje diciendo que todo salio como lo esperado
		$msg = "Persona agregada correctamente";
	} else { 
		// Si hay un dato sin rellenar mostramos el siguiente texto.
		$msg = "Falta rellenar algun dato"; 
	}
}
if($_POST['volver']){
	header('Location: index.html');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agenda - Agregar personas</title>
</head>
<style type="text/css"> 
body{background-color:#617AB7 }
h1{color:#222F4E;text-align:left}
.agenda {
	margin:100px auto 0 auto; 
	width:701px;
	height:468px;
	background-image:url(imagenes/agenda.jpg);
}
.agenda #contenidor {
	padding:25px;
	width:276px;
	height:428px;
}
td{color:#222F4E}
.falta{position:absolute;top:395px; left:380px;}
.nombre{position:absolute;top:215px; left:380px;}
#nombre{position:absolute;top:235px; left:380px;}
.apellidos{position:absolute;top:275px; left:380px;}
#apellidos{position:absolute;top:295px; left:380px;}
.direccion{position:absolute;top:335px; left:380px;}
#direccion{position:absolute;top:355px; left:380px;}
.mail{position:absolute;top:215px; left:760px;}
#email{position:absolute;top:235px; left:760px;}
.telefono{position:absolute;top:275px; left:760px;}
#telefono{position:absolute;top:295px; left:760px;}
.foto{position:absolute;top:335px; left:760px;}
#foto{position:absolute;top:355px; left:760px;}
.bagen{position:absolute;top:395px; left:760px;color:#222F4E;}
.bvolv{position:absolute;top:395px; left:840px;color:#222F4E;}

</style>
<body>
<div class="agenda">
	<div id="contenidor">
	  <table width="230%" height="404" border="0" >
	    <tr>
	      <td height="38" colspan="3" align="center" valign="middle"><h1>Agregar Persona</h1></td>
        </tr>
	    <tr>
	      <td colspan="3" valign="top"><em><span class="falta" style="color:red;"><?=$msg;?></span></em>
          <form action="agregar.php" method="post">
	        <strong class="nombre">Nombre</strong><br />
          <input type="text" name="nombre" id="nombre" />
          <br />
		  <br />
          <strong class="apellidos">Apellidos</strong>
           <input type="text" name="apellidos" id="apellidos" />
          <br />
          <br />
          <strong class="direccion">Dirección</strong><br />
          <input type="text" name="direccion" id="direccion" />
          <br />
          <br />
          <strong class="mail">E-m@il</strong><br />
          <input type="text" name="email" id="email" />
          <br />
          <br />
          <strong class="telefono">Telefono</strong><br />
          <input type="text" name="telefono" id="telefono" />
          <br />
          <br />
          <strong class="foto">Foto</strong><br />
          <input type="text" name="foto" id="foto" />
          <br />
          <br />
          
		  <input type="submit" name="agendar" value="Agregar" class="bagen"/>		
		<input type="submit" name="volver" value="Inicio" class="bvolv"/>

		

        
          </td>
        </tr>
      </table>		
  </div>
  	</form>
</div>
</body>
</html>
