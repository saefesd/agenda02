AGENDA PHP
###########

Creada por Marco A. Sanchez
----------------------------

	Esta es una agenda realizada en PHP. Se puden guardar registros con los campos nombre,apellidos,direccion,
	email,telefono y puede guardar una foto. Ademas busca filtrando por nombre o muestra todos los registros, pudiendo editar 
	los registros guardados con anterioridad.Tambien se pueden borrar los registros de forma individual o todos los datos de la tabla.

	*	Instalacion:
	-----------------
		1-. Para poder hacer funcionar nuestra agenda debemos de instalar un servidor web, en este caso apache.Tambien necesitamos 
		instalar mysql y phpmyadmin y compatibilidad con PHP con lo cual instalaremos LAMP. Dejo mi turorial de instalacion:
	
		http://www.saefesd.infenlaces.com/trabajos/despliegue/practicaconf.zip
		
		2-. En el archivo de configuracion (config.php) estan los datos necesario para el correcto funcionamiento. Podemos cambiar los 
		datos alli, en ese archivo o poner los datos que en el aparecen, tales como el nombre de usuario  contraseña en phpmyadmin, el
		nombre de la base de datos mysql. 
		
		3-. Una vez realizado el paso anterior creando la base de datos con el nombre de "personas" vamos a la pestaña "SQL" y copiamos
		el contenido del archivo sql.txt. 
		
		4-. Colocaremos la carpeta de la agenda en la carpeta public_html dentro de home
		
		5-. Ya deberia de funcionar. 
		
	*	Uso:
	--------
	
		El programa arranca desde el index.html, donde esta el repositorio del proyecto en bitbucket.
			
		https://bitbucket.org/saefesd/agenda02/overview
			
		Aparecen 3 imagenes que ejercen de menus. La primera imagen nos llevara al formulario de agregar contacto. Los campos que 
		podemos agregar del contacto son Nombre, apellidos, direccion, email, telefono y la url de una fotografia. Los 5 primeros 
		campos son obligatorios. Si al menos esos 5 campos estan rellenados al presionar el boton de agregar se guardara el 
		registro de contacto en la base de datos mysql en la tabla "personas". Ademas tenemos otro boton para volver al index.html.
		
		La 2º imagen redirecciona al formulario de buscar. Tenemos una caja de texto para introducir el nombre de contacto y al 
		presionar el boton de buscar si existe un contacto con ese nombre aparecera debajo el nombre del contacto. Si presionamos
		el nombre apareceran todos los datos de ese contacto que podremos editar. Si abrimos la edicion del contacto podremos ademas
		borrar ese contacto de la base de datos.
		
		La 3º imagen redirecciona al formulario de listado. Apareceran el nombre, apellidos y email de todos los contactos de
		nuestra base de datos. En este formulario podremos borrar todos los datos de nuestra base de datos.
	
		
	
		