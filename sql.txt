CREATE TABLE `personas` (
  `id` int(8) NOT NULL auto_increment,
  `nombre` varchar(50) default NULL,
  `apellidos` varchar(50) default NULL,
  `direccion` varchar(50) default NULL,
  `email` varchar(50) default NULL,
  `telefono` int(10) default NULL,
  `foto` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;